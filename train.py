import copy
from functools import partial
import hydra
import jax
import jax.experimental
import jax.numpy as np
import optax
import torch
import wandb
from flax.training import train_state
from omegaconf import DictConfig, OmegaConf
from tqdm import tqdm
from data import Datasets
from adversary import *
from utilities import *

# constructor of the trainable weights and learning_rate schedule
def create_train_state(
    rng,
    model_cls,
    trainloader,
    lr=1e-3,
    lr_layer=None,
    lr_schedule=False,
    weight_decay=0.0,
    total_steps=-1,
):
    model = model_cls(training=True)
    init_rng, dropout_rng = jax.random.split(rng, num=2)
    params = model.init(
        {"params": init_rng, "dropout": dropout_rng},
        np.array(next(iter(trainloader))[0].numpy()),
    )
    
    params = params["params"]

    if lr_schedule:
        schedule_fn = lambda lr: optax.cosine_onecycle_schedule(
            peak_value=lr,
            transition_steps=total_steps,
            pct_start=0.1,
        )
    else:
        schedule_fn = lambda lr: lr
    # lr_layer is a dictionary from parameter name to LR multiplier
    if lr_layer is None:
        lr_layer = {}

    optimizers = {
        k: optax.adam(learning_rate=schedule_fn(v * lr))
        for k, v in lr_layer.items()
    }

    optimizers["__default__"] = optax.adamw(
        learning_rate=schedule_fn(lr),
        weight_decay=weight_decay,
    )
    name_map = map_nested_fn(lambda k, _: k if k in lr_layer else "__default__")
    tx = optax.multi_transform(optimizers, name_map)

    extra_keys = set(lr_layer.keys()) - set(jax.tree_leaves(name_map(params)))
    assert (
       len(extra_keys) == 0
    ), f"Special params {extra_keys} do not correspond to actual params"

    # Print parameter count
    _is_complex = lambda x: x.dtype in [np.complex64, np.complex128]
    param_sizes = map_nested_fn(
        lambda k, param: param.size * (2 if _is_complex(param) else 1)
        if lr_layer.get(k, lr) > 0.0
        else 0
    )(params)
    print(f"[*] Trainable Parameters: {sum(jax.tree_leaves(param_sizes))}")
    print(f"[*] Total training steps: {total_steps}")

    return train_state.TrainState.create(
        apply_fn=model.apply, params=params, tx=tx
    )

# a helper function for the gradient computation used by the robust training
@partial(jax.jit, static_argnums=(2,))
def max_grad_X(params, dropout_rng, model, X, y, index):
    def max_loss_f(X):
        output = model.apply({"params": params}, X, training=False, rngs={'dropout': dropout_rng})
        maxLoss = (output.take(index, 1) - output.take(y, 1)).mean()
        return maxLoss
    grad_function = jax.grad(max_loss_f)
    gradient = grad_function(X)
    return gradient

def train_epoch(state, rng, model, trainloader, n_classes=10, classification=False, robust=False, robust_alg=1, robust_iter=10, robust_step_size=0.05, X_bounds=None):
    model = model(training=True)
    batch_losses, batch_accuracies = [], []
    for batch_idx, (inputs, labels) in enumerate(tqdm(trainloader)):
        inputs = np.array(inputs.numpy())
        labels = np.array(labels.numpy()) 
        rng, drop_rng = jax.random.split(rng)
        if robust:  # preparation of the modified samples for the robust training
            eps = (X_bounds[1] - X_bounds[0])/5
            X = inputs
            N = X.shape[0]
            C = X.shape[-1]
            y = labels
            # replicate the data NumberOfClasses-times
            X_rep = X.repeat(n_classes, axis=0).reshape(n_classes*N, -1, C)
            y_rep = y.reshape(-1, 1).repeat(n_classes, axis=0).flatten()
            X_original = copy.deepcopy(X_rep)
            index = torch.tensor([jj for jj in range(n_classes)] * N).view(-1, 1).long().numpy()
            # simulate the adversarial data samples
            for i in range(robust_iter):
                gradient = max_grad_X(state.params, drop_rng, model, X_rep, y_rep, index) 
                X_rep = X_rep + robust_step_size * np.sign(gradient)
                X_rep = X_rep.clip(min=X_bounds[0], max=X_bounds[1])
                X_rep = X_original + (X_rep-X_original).clip(min=-eps, max=eps)
            if robust_alg == 2:
                X_out = np.concatenate((X, X_rep), axis=0)
                y_out = np.concatenate((y, y_rep), axis=0)
            else:
                X_out = X_rep
                y_out = y_rep
            state, loss, acc = train_step(
                state,
                drop_rng,
                X_out,
                y_out,
                model,
                classification=classification,
            )
        else:   # the classic training
            state, loss, acc = train_step(
                state,
                drop_rng,
                inputs,
                labels,
                model,
                classification=classification,
            )
        batch_losses.append(loss)
        batch_accuracies.append(acc)

    # Return average loss over batches
    return (
        state,
        np.mean(np.array(batch_losses)),
        np.mean(np.array(batch_accuracies)),
    )

# modify the weights values by the SGD gradient
@partial(jax.jit, static_argnums=(4, 5))
def train_step(
    state, rng, batch_inputs, batch_labels, model, classification=False
):
    def loss_fn(params):
        logits, mod_vars = model.apply(
            {"params": params},
            batch_inputs,
            rngs={"dropout": rng},
            mutable=["intermediates"],
        )
        loss = np.mean(cross_entropy_loss(logits, batch_labels))
        acc = np.mean(compute_accuracy(logits, batch_labels))
        return loss, (logits, acc)

    if not classification:
        batch_labels = batch_inputs[:, :, 0]

    grad_fn = jax.value_and_grad(loss_fn, has_aux=True)
    (loss, (logits, acc)), grads = grad_fn(state.params)
    state = state.apply_gradients(grads=grads)
    return state, loss, acc

# the main train function, takes care of the whole training phase
def example_train(
    dataset: str,
    layer: str,
    seed: int,
    train: DictConfig,
    model: DictConfig,
    trainloader, testloader,
    model_cls,
    robust:bool = False,
    X_bounds:tuple[int] = None
):
    # Set randomness...
    print("[*] Setting Randomness...")
    torch.random.manual_seed(seed)  # For dataloader order
    key = jax.random.PRNGKey(seed)
    key, rng, train_rng = jax.random.split(key, num=3) #np.array[x, y] for each of key, rng, train_rng

    # Check if classification dataset
    classification = "classification" in dataset

    # Get model class and arguments
    layer_cls = Models[layer]

    # Extract custom hyperparameters from model class
    lr_layer = getattr(layer_cls, "lr", None)

    if robust:
        print("Robust learning is active")
    print(f"[*] Starting `{layer}` Training on `{dataset}` =>> Initializing...")

    state = create_train_state(
        rng,
        model_cls,
        trainloader,
        lr=train.lr,
        lr_layer=lr_layer,
        lr_schedule=train.lr_schedule,
        weight_decay=train.weight_decay,
        total_steps=len(trainloader) * train.epochs,
    )

    # Loop over epochs
    best_loss, best_acc, best_epoch = 10000, 0, 0
    for epoch in range(train.epochs):
        print(f"[*] Starting Training Epoch {epoch + 1}...")
        state, train_loss, train_acc = train_epoch(
            state,
            train_rng,
            model_cls,
            trainloader,
            classification=classification,
            robust=robust,
            robust_alg=train["robust_alg"],
            robust_iter=train["robust_iters"],
            robust_step_size=train["robust_step_size"],
            X_bounds=X_bounds
        )

        print(f"[*] Running Epoch {epoch + 1} Validation...")
        test_loss, test_acc = validate(
            state.params, model_cls, testloader, classification=classification
        )

        print(f"\n=>> Epoch {epoch + 1} Metrics ===")
        print(
            f"\tTrain Loss: {train_loss:.5f} -- Train Accuracy:"
            f" {train_acc:.4f}\n\t Test Loss: {test_loss:.5f} --  Test"
            f" Accuracy: {test_acc:.4f}"
        )

        if (classification and test_acc > best_acc) or (
            not classification and test_loss < best_loss
        ):
            best_loss, best_acc, best_epoch = test_loss, test_acc, epoch

        # Print best accuracy & loss so far...
        print(
            f"\tBest Test Loss: {best_loss:.5f} -- Best Test Accuracy:"
            f" {best_acc:.4f} at Epoch {best_epoch + 1}\n"
        )

        if wandb is not None:
            wandb.log(
                {
                    "train/loss": train_loss,
                    "train/accuracy": train_acc,
                    "test/loss": test_loss,
                    "test/accuracy": test_acc,
                },
                step=epoch,
            )
            wandb.run.summary["Best Test Loss"] = best_loss
            
            wandb.run.summary["Best Test Accuracy"] = best_acc
            wandb.run.summary["Best Epoch"] = best_epoch
    return state.params

@hydra.main(version_base=None, config_path="", config_name="config")
def main(cfg: DictConfig) -> None:
    #print(OmegaConf.to_yaml(cfg))
    print("cfg: ", cfg)
    
    OmegaConf.set_struct(cfg, False)  # Allow writing keys

    # Track with wandb
    if wandb is not None:
        wandb_cfg = cfg.pop("wandb")
        wandb.init(
            **wandb_cfg, config=OmegaConf.to_container(cfg, resolve=True)
        )

    create_dataset_fn = Datasets[cfg.dataset]
    trainloader, testloader, n_classes, l_max, d_input, X_bounds = create_dataset_fn(
        bsz=cfg.train.bsz, series=True#, soundlen=1000
    )

    model_cls = generate_model_cls(cfg.layer, n_classes, l_max, True, cfg.model)
    #params = load_model_params("/home/grifon/diplomka/diplomka-progress/robust-s4-unified/s4-500k-robust_params")
    params = example_train(robust=True, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
    #save_model_params(params, "saves/gtsrb_s4")
    pgdAttackTest(params, model_cls, testloader, X_bounds, show_image=False)
    fgsmAttackTest(params, model_cls, testloader, show_image=False)
    spatialAttackTest(params, model_cls, testloader, X_bounds, serial_data=True, show_image=True)

if __name__ == "__main__":
    main()
