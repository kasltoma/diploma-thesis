from functools import partial
import jax
import jax.numpy as np
import matplotlib.pyplot as plt
from omegaconf import DictConfig, OmegaConf
import pickle
import shutil
import yaml
from feedforward import FeedForwardModel
from tqdm import tqdm
from lstm import LSTMRecurrentModel
from recurrent import RecurrentModel
from s4 import BatchStackedModel, S4Layer, SSMLayer

# list of accessible models of NN
Models = {
    "ff": FeedForwardModel,
    "lstm": LSTMRecurrentModel,
    "rnn": RecurrentModel,
    "ssm": SSMLayer,
    "s4": S4Layer,
}

# constructor of the traininable neural network
def generate_model_cls(
    layer: str,
    n_classes: int,
    l_max: int,
    classification: bool,
    model: DictConfig,
    ):
    layer_cls = Models[layer]
    model.layer.l_max = l_max
    model_cls = partial(
        BatchStackedModel,
        layer_cls=layer_cls,
        d_output=n_classes,
        classification=classification,
        **model,
    )
    return model_cls

#
# Following is implementation of the neccesary NN training functions
#
@partial(np.vectorize, signature="(c),()->()")
def cross_entropy_loss(logits, label):
    one_hot_label = jax.nn.one_hot(label, num_classes=logits.shape[0])
    return -np.sum(one_hot_label * logits)

@partial(np.vectorize, signature="(c),()->()")
def compute_accuracy(logits, label):
    return np.argmax(logits) == label

def validate(params, model, testloader, classification=False):
    # Compute average loss & accuracy
    model = model(training=False)
    losses, accuracies = [], []
    for batch_idx, (inputs, labels) in enumerate(tqdm(testloader)):
        inputs = np.array(inputs.numpy())
        labels = np.array(labels.numpy())  # Not the most efficient...
        loss, acc = eval_step(
            inputs, labels, params, model, classification=classification
        )
        losses.append(loss)
        accuracies.append(acc)

    return np.mean(np.array(losses)), np.mean(np.array(accuracies))

@partial(jax.jit, static_argnums=(3, 4))
def eval_step(batch_inputs, batch_labels, params, model, classification=False):

    if not classification:
        batch_labels = batch_inputs[:, :, 0]
    logits = model.apply({"params": params}, batch_inputs)
    loss = np.mean(cross_entropy_loss(logits, batch_labels))
    acc = np.mean(compute_accuracy(logits, batch_labels))
    return loss, acc

def map_nested_fn(fn):
    """Recursively apply `fn to the key-value pairs of a nested dict / pytree."""

    def map_fn(nested_dict):
        return {
            k: (map_fn(v) if hasattr(v, "keys") else fn(k, v))
            for k, v in nested_dict.items()
        }

    return map_fn

# saves both the used config file (as text) and trained weights (as pickle)
def save_model_params(params, filename:str = None):
    shutil.copy('config.yaml', filename+"_config.yaml")
    with open(filename, "wb") as file:
        pickle.dump(params, file)

# loads both the used config file (as text) and trained weights (as pickle)
def load_model_params(filename):
    with open(filename+"_config.yaml", "r") as yamlfile:
        config = OmegaConf.create( yaml.load(yamlfile, Loader=yaml.FullLoader) )
    with open(filename, "rb") as file:
        params = pickle.load(file)
    return config, params

# functions for rendering image data samples
def plot_image(X, W, H, C, title=""):
    X = normalize_pic_for_plot(X)
    plt.imshow(X.reshape(W,H,C), cmap='gray')
    plt.title()
    plt.show()

def plot_images_trio(X_original, X, W, H, C): 
    X_original, X = normalize_pic_for_plot(X_original, X)
    fig, axes = plt.subplots(1, 3, figsize=(15, 5)) 
    axes[0].imshow(X_original.reshape(W,H,C))
    axes[0].set_title("original")
    axes[1].imshow(X.reshape(W,H,C))
    axes[1].set_title("modified")
    axes[2].imshow( (X_original-X).reshape(W,H,C))
    axes[2].set_title("difference")
    plt.show()

def normalize_pic_for_plot(X_original, X):
    T = np.array( [X_original, X])
    lb = np.min(T)
    T_new = T - lb
    ub = np.max(T_new)
    T_ret = T_new / ub
    return T_ret[0], T_ret[1]
