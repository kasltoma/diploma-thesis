
# Diploma Thesis
Min-max optimization methods inadversarial learning  - Robust learning of Neural Network models, including S4at FEL ČVUT / FEE CTU in Prague, bc. Tomáš Kasl

### The thesis document

can be seen [Here:](https://gitlab.fel.cvut.cz/kasltoma/diploma-thesis/-/blob/main/thesis.pdf)

### Final presentation:

can be seen [Here:](TBD)

### The robust training can be run,:
in [main.py:](https://gitlab.fel.cvut.cz/kasltoma/diploma-thesis/-/blob/main/main.py)
or in [compare_models_on_robustness.py]

Based on [annotated S4](https://srush.github.io/annotated-s4/) and [solving optimization problem](https://github.com/optimization-for-data-driven-science/Robust-NN-Training)
