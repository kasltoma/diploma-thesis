import hydra
from omegaconf import DictConfig, OmegaConf
import wandb

import numpy
import pandas as pd
import os
import librosa
#import soundata
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Conv1D, MaxPooling1D
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms

from data import Datasets
from lstm import LSTMRecurrentModel
from s4 import BatchStackedModel, S4Layer, SSMLayer
from train import *
from utilities import *

# 0 = air_conditioner
# 1 = car_horn
# 2 = children_playing
# 3 = dog_bark
# 4 = drilling
# 5 = engine_idling
# 6 = gun_shot
# 7 = jackhammer
# 8 = siren
# 9 = street_music

class CustomDataset(Dataset):
    def __init__(self, X, y, transform=None):
        self.X = X
        self.y = y
        self.transform = transform
        
    def __len__(self):
        return len(self.X)
    
    def __getitem__(self, idx):
        sample = {'feature': self.X[idx], 'label': self.y[idx]}
        sample, label = self.X[idx], self.y[idx]
        
        if self.transform:
            sample= self.transform(sample)
            #print("vracim pic: ", sample)
            #print(sample.shape)
        return sample, label


def load_data(data_path, metadata_path, n_sounds=9000):
    features = []
    labels = []

    metadata = pd.read_csv(metadata_path)
    max_len = 0

    for index, row in metadata.iterrows():
        if index >= n_sounds:
            break
        print("loading file", index)
        file_path = os.path.join(data_path, f"fold{row['fold']}", f"{row['slice_file_name']}")

        # Load the audio file and resample it
        #target_sr = 22050
        target_sr = 44100
        audio, sample_rate = librosa.load(file_path, sr=target_sr)

        # Extract MFCC features
        mfccs = librosa.feature.mfcc(y=audio, sr=target_sr, n_mfcc=128)
        mfccs_scaled = numpy.mean(mfccs.T, axis=0)
        n = len(mfccs_scaled)
        if n > max_len:
            max_len = n

        # Append features and labels
        features.append(mfccs_scaled)
        labels.append(row['class'])


    return numpy.array(features), numpy.array(labels), max_len




@hydra.main(version_base=None, config_path="", config_name="config")
def main(cfg: DictConfig) -> None:
    print(OmegaConf.to_yaml(cfg))
    OmegaConf.set_struct(cfg, False)  # Allow writing keys

    # Track with wandb
    if wandb is not None:
        wandb_cfg = cfg.pop("wandb")
        wandb.init(
            **wandb_cfg, config=OmegaConf.to_container(cfg, resolve=True)
        )

    data_path = "/home/grifon/diplomka/diplomka-progress/robust-s4-unified/data/UrbanSound8K/audio"
    metadata_path = "/home/grifon/diplomka/diplomka-progress/robust-s4-unified/data/UrbanSound8K/metadata/UrbanSound8K.csv"
    features, labels, max_len = load_data(data_path, metadata_path)


    # Encode labels
    le = LabelEncoder()
    labels_encoded = le.fit_transform(labels)
    labels_onehot = to_categorical(labels_encoded)

    X_train, X_test, y_train, y_test = train_test_split(features, labels_onehot, test_size=0.2, random_state=42)
    print("X_train: ", X_train.shape)
    #X_train = X_train.reshape(-1, 1).T
    X_train = X_train.reshape(X_train.shape[0], X_train.shape[1], 1)
    X_test = X_test.reshape(X_test.shape[0], X_test.shape[1], 1)
    print("X_train po reshape: ", X_train.shape)
    X_train = [ x for x in X_train ]
    y_train = numpy.argmax(y_train, axis=1)
    X_test = [ x for x in X_test ]
    y_test = numpy.argmax(y_test, axis=1)
    #y_train = [y for y in y_train.nump]

    tf = transforms.Compose(
        [
            transforms.ToTensor(),
            #transforms.Normalize(mean=0.5, std=0.5),
            transforms.Lambda(lambda x: x.view(1, -1).t()),
        ]
    )

    train_dataset = CustomDataset(X_train, y_train, transform=tf)
    test_dataset = CustomDataset(X_test, y_test, transform=tf)

    trainloader = torch.utils.data.DataLoader(
        train_dataset, batch_size=16, shuffle=True
    )
    testloader = torch.utils.data.DataLoader(
        test_dataset, batch_size=16, shuffle=False
    )
    print("maxlen = ", max_len)
    model_cls = generate_model_cls(cfg.layer, 10, max_len, True, cfg.model)
    #params = load_model_params("/home/grifon/diplomka/diplomka-progress/robust-s4-unified/s4-500k-robust_params")
    params = example_train(robust=False, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)

if __name__ == "__main__":
    main()