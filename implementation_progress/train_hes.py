# https://jax.readthedocs.io/en/latest/_autosummary/jax.hessian.htmlhttps://jax.readthedocs.io/en/latest/_autosummary/jax.hessian.html
import time


import os
import copy
import shutil
from functools import partial
import hydra
import jax
import jax.numpy as np
import optax
import torch
from flax import linen as nn
from flax.training import checkpoints, train_state
from omegaconf import DictConfig, OmegaConf
from tqdm import tqdm
from data import Datasets
#from .dss import DSSLayer
from s4 import BatchStackedModel, S4Layer, SSMLayer, sample_image_prefix
#from .s4 import S4Layer, sample_image_prefix
#from .s4d import S4DLayer
from adversary import pgdAttackTest

try:
    # Slightly nonstandard import name to make config easier - see example_train()
    import wandb

    assert hasattr(wandb, "__version__")  # verify package import not local dir
except (ImportError, AssertionError):
    wandb = None

# ## Baseline Models
#
# We start with definitions of various models we're already familiar with, starting with a feed-forward
# (history-blind) projection model, followed by a strong LSTM-based recurrent baseline.

# ### Utilities
# We define a couple of utility functions below to compute a standard cross-entropy loss, and compute
# "token"-level prediction accuracy.


@partial(np.vectorize, signature="(c),()->()")
def cross_entropy_loss(logits, label):
    one_hot_label = jax.nn.one_hot(label, num_classes=logits.shape[0])
    return -np.sum(one_hot_label * logits)




@partial(np.vectorize, signature="(c),()->()")
def compute_accuracy(logits, label):
    return np.argmax(logits) == label


# As we're using Flax, we also write a utility function to return a default TrainState object.
# This function initializes model parameters, as well as our optimizer. Note that for S4 models,
# we use a custom learning rate for parameters of the S4 kernel (lr = 0.001, no weight decay).
def map_nested_fn(fn):
    """Recursively apply `fn to the key-value pairs of a nested dict / pytree."""

    def map_fn(nested_dict):
        return {
            k: (map_fn(v) if hasattr(v, "keys") else fn(k, v))
            for k, v in nested_dict.items()
        }

    return map_fn


def create_train_state(
    rng,
    model_cls,
    trainloader,
    lr=1e-3,
    lr_layer=None,
    lr_schedule=False,
    weight_decay=0.0,
    total_steps=-1,
):
    model = model_cls(training=True)
    init_rng, dropout_rng = jax.random.split(rng, num=2)
    params = model.init(
        {"params": init_rng, "dropout": dropout_rng},
        np.array(next(iter(trainloader))[0].numpy()),
    )
    # Note: Added immediate `unfreeze()` to play well w/ Optax. See below!
    # params = params["params"].unfreeze() #TADY
    params = params["params"]

    # Handle learning rates:
    # - LR scheduler
    # - Set custom learning rates on some SSM parameters

    # Note for Debugging... this is all undocumented and so weird. The following links are helpful...
    #
    #   > Flax "Recommended" interplay w/ Optax (this bridge needs ironing):
    #       https://github.com/google/flax/blob/main/docs/flip/1009-optimizer-api.md#multi-optimizer
    #
    #   > But... masking doesn't work like the above example suggests!
    #       Root Explanation: https://github.com/deepmind/optax/issues/159
    #       Fix: https://github.com/deepmind/optax/discussions/167
    #
    #   > Also... Flax FrozenDict doesn't play well with rest of Jax + Optax...
    #       https://github.com/deepmind/optax/issues/160#issuecomment-896460796
    #
    #   > Solution: Use Optax.multi_transform!

    if lr_schedule:
        schedule_fn = lambda lr: optax.cosine_onecycle_schedule(
            peak_value=lr,
            transition_steps=total_steps,
            pct_start=0.1,
        )
    else:
        schedule_fn = lambda lr: lr
    # lr_layer is a dictionary from parameter name to LR multiplier
    if lr_layer is None:
        lr_layer = {}

    optimizers = {
        k: optax.adam(learning_rate=schedule_fn(v * lr))
        for k, v in lr_layer.items()
    }
    # Add default optimizer
    # Note: it would be better to use a dummy key such as None that can't conflict with parameter names,
    # but this causes a hard-to-trace error; it seems that the transforms keys list is being sorted inside optax.multi_transform
    # which causes an error since None can't be compared to str
    optimizers["__default__"] = optax.adamw(
        learning_rate=schedule_fn(lr),
        weight_decay=weight_decay,
    )
    name_map = map_nested_fn(lambda k, _: k if k in lr_layer else "__default__")
    tx = optax.multi_transform(optimizers, name_map)
    # For debugging, this would be the default transform with no scheduler or special params
    # tx = optax.adamw(learning_rate=lr, weight_decay=0.01)

    # Check that all special parameter names are actually parameters
    extra_keys = set(lr_layer.keys()) - set(jax.tree_leaves(name_map(params)))
    assert (
        len(extra_keys) == 0
    ), f"Special params {extra_keys} do not correspond to actual params"

    # Print parameter count
    _is_complex = lambda x: x.dtype in [np.complex64, np.complex128]
    param_sizes = map_nested_fn(
        lambda k, param: param.size * (2 if _is_complex(param) else 1)
        if lr_layer.get(k, lr) > 0.0
        else 0
    )(params)
    print(f"[*] Trainable Parameters: {sum(jax.tree_leaves(param_sizes))}")
    print(f"[*] Total training steps: {total_steps}")

    return train_state.TrainState.create(
        apply_fn=model.apply, params=params, tx=tx
    )


# We also use this opportunity to write generic train_epoch and validation functions. These functions generally
# operate by taking in a training state, model class, dataloader, and critically, the model-specific step function.
# We define the step functions on a model-specific basis below.

def max_grad_X(params, model, X, y, index):
    def max_loss_f(X):
        output = model.apply({"params": params}, X)
        #maxLoss = (output.gather(1, index) - output.gather(1, y)).mean()
        maxLoss = (output.take(index, 1) - output.take(y, 1)).mean()
        return maxLoss
    grad_function = jax.grad(max_loss_f)
    gradient = grad_function(X)
    return gradient

def max_hes_X(params, model, X, y, index):
    def max_loss_f(X):
        output = model.apply({"params": params}, X)
        #maxLoss = (output.gather(1, index) - output.gather(1, y)).mean()
        maxLoss = (output.take(index, 1) - output.take(y, 1)).mean()
        return maxLoss
    hes_function = jax.hessian(max_loss_f)
    hessian = hes_function(X)
    return hessian

def train_epoch(state, rng, model, trainloader, classification=False, robust=False):
    # Store Metrics
    model = model(training=True)
    batch_losses, batch_accuracies = [], []
    for batch_idx, (inputs, labels) in enumerate(tqdm(trainloader)):
        #print("batch: ", batch_idx)
        inputs = np.array(inputs.numpy())
        labels = np.array(labels.numpy())  # Not the most efficient...
        rng, drop_rng = jax.random.split(rng)
        if robust:
            # prepare robust values
            #MaxIter_max = 11
            MaxIter_max = 10
            #step_size_max = 0.1
            step_size_max = 0.2
            eps = 0.4
            X = inputs
            N = X.shape[0]
            y = labels
            X_rep = X.repeat(10, axis=0).reshape(10 *N, -1, 1)
            y_rep = y.reshape(-1, 1).repeat(10, axis=0).flatten()
            X_original = copy.deepcopy(X_rep)
            index = torch.tensor([jj for jj in range(10)] * N).view(-1, 1).long().numpy()

            gradient = max_grad_X(state.params, model, X, y, index) 
            gradient = max_grad_X(state.params, model, X, y, index) 
            hessian = max_hes_X(state.params, model, X, y, index) 
            hessian = max_hes_X(state.params, model, X, y, index) 


            start_time_sec = time.time()
            for i in range(MaxIter_max):
                gradient = max_grad_X(state.params, model, X, y, index) #TODO X_grad = torch.autograd.grad(maxLoss, X, retain_graph=True)[0]
            end_time_sec = time.time()
            seconds_taken = end_time_sec - start_time_sec
            print("Gradient time taken in seconds:", seconds_taken)
            print("Gradient shape: ", gradient.shape)
            start_time_sec = time.time()
            for i in range(MaxIter_max):
                hessian = max_hes_X(state.params, model, X, y,  index) #TODO X_grad = torch.autograd.grad(maxLoss, X, retain_graph=True)[0]
            end_time_sec = time.time()
            seconds_taken = end_time_sec - start_time_sec
            print("Hessian time taken in seconds:", seconds_taken)
            print("Hessian shape: ", hessian.shape)
            exit(1)
                #X_rep = X_rep + step_size_max * np.sign(gradient)
                #X_rep = X_original + (X_rep-X_original).clip(min=-eps, max=eps)
                #X_rep = X_rep.clip(min=-1, max=0)
            state, loss, acc = train_step(
                state,
                drop_rng,
                X_rep,
                y_rep,
                model,
                classification=classification,
            )
        else:
            state, loss, acc = train_step(
                state,
                drop_rng,
                inputs,
                labels,
                model,
                classification=classification,
            )
        batch_losses.append(loss)
        batch_accuracies.append(acc)

    # Return average loss over batches
    return (
        state,
        np.mean(np.array(batch_losses)),
        np.mean(np.array(batch_accuracies)),
    )


def validate(params, model, testloader, classification=False):
    # Compute average loss & accuracy
    model = model(training=False)
    losses, accuracies = [], []
    for batch_idx, (inputs, labels) in enumerate(tqdm(testloader)):
        inputs = np.array(inputs.numpy())
        labels = np.array(labels.numpy())  # Not the most efficient...
        loss, acc = eval_step(
            inputs, labels, params, model, classification=classification
        )
        losses.append(loss)
        accuracies.append(acc)

    return np.mean(np.array(losses)), np.mean(np.array(accuracies))


# ### Feed-Forward Model
# Here, we establish a skeleton for a simple, history-blind feed-forward model. For each element $x_t$ of a sequence, our
# feed-forward model attempts to predict $x_{t+1}$. During generation, the predicted "token" is fed as the new current
# element.


class FeedForwardModel(nn.Module):
    d_model: int
    N: int
    l_max: int
    decode: bool = False

    def setup(self):
        self.dense = nn.Dense(self.d_model)

    def __call__(self, x):
        """x - L x N"""
        return nn.relu(self.dense(x))


# We define separate step functions for running training and evaluation steps, accordingly. These step functions are
# each wrapped in a call to `@jax.jit` which fuses operations, generally leading to high performance gains. These @jit
# calls will become increasingly important as we optimize S4.


@partial(jax.jit, static_argnums=(4, 5))
def train_step(
    state, rng, batch_inputs, batch_labels, model, classification=False
):
    def loss_fn(params):
        logits, mod_vars = model.apply(
            {"params": params},
            batch_inputs,
            rngs={"dropout": rng},
            mutable=["intermediates"],
        )
        loss = np.mean(cross_entropy_loss(logits, batch_labels))
        acc = np.mean(compute_accuracy(logits, batch_labels))
        return loss, (logits, acc)

    if not classification:
        batch_labels = batch_inputs[:, :, 0]

    grad_fn = jax.value_and_grad(loss_fn, has_aux=True)
    (loss, (logits, acc)), grads = grad_fn(state.params)
    state = state.apply_gradients(grads=grads)
    return state, loss, acc

"""

def grad_X(params, model, X, y):
    def loss_f(X):
        logits = model.apply({"params": params}, X)
        loss = np.mean(cross_entropy_loss(logits, y))
        return loss
    grad_function = jax.grad(loss_f)
    gradient = grad_function(X)
    return gradient


import jax.numpy as jnp
from jax import vjp

# Define your scalar-valued function
def my_function(x):
    return jnp.sum(x ** 2)

# Example input
x = jnp.array([1.0, 2.0, 3.0])

# Compute the vector-Jacobian product
_, grad_fn = vjp(my_function, x)

# Compute the gradient
gradient = grad_fn(jnp.ones_like(x))

print("Gradient of my_function with respect to x:", gradient)
"""

@partial(jax.jit, static_argnums=(3, 4))
def eval_step(batch_inputs, batch_labels, params, model, classification=False):
    if not classification:
        batch_labels = batch_inputs[:, :, 0]
    logits = model.apply({"params": params}, batch_inputs)
    loss = np.mean(cross_entropy_loss(logits, batch_labels))
    acc = np.mean(compute_accuracy(logits, batch_labels))
    return loss, acc

Models = {
    #"ff": FeedForwardModel,
    "s4": S4Layer,
}


def example_train(
    dataset: str,
    layer: str,
    seed: int,
    model: DictConfig,
    train: DictConfig,
    robust:bool = False
):
    # Warnings and sanity checks
    if not train.checkpoint: #TODO add checkpoint via parameters
        print("[*] Warning: models are not being checkpoint")

    # Set randomness...
    print("[*] Setting Randomness...")
    torch.random.manual_seed(seed)  # For dataloader order
    key = jax.random.PRNGKey(seed)
    key, rng, train_rng = jax.random.split(key, num=3) #np.array[x, y] for each of key, rng, train_rng

    # Check if classification dataset
    classification = "classification" in dataset

    # Create dataset
    create_dataset_fn = Datasets[dataset]
    trainloader, testloader, n_classes, l_max, d_input = create_dataset_fn(
        bsz=train.bsz
    )

    # Get model class and arguments
    layer_cls = Models[layer]
    model.layer.l_max = l_max

    # Extract custom hyperparameters from model class
    lr_layer = getattr(layer_cls, "lr", None)

    if robust:
        print("Robust learning is active")
    print(f"[*] Starting `{layer}` Training on `{dataset}` =>> Initializing...")

    model_cls = partial(
        BatchStackedModel,
        layer_cls=layer_cls,
        d_output=n_classes,
        classification=classification,
        **model,
    )

    state = create_train_state(
        rng,
        model_cls,
        trainloader,
        lr=train.lr,
        lr_layer=lr_layer,
        lr_schedule=train.lr_schedule,
        weight_decay=train.weight_decay,
        total_steps=len(trainloader) * train.epochs,
    )

    # Loop over epochs
    best_loss, best_acc, best_epoch = 10000, 0, 0
    for epoch in range(train.epochs):
        print(f"[*] Starting Training Epoch {epoch + 1}...")
        state, train_loss, train_acc = train_epoch(
            state,
            train_rng,
            model_cls,
            trainloader,
            classification=classification,
            robust=robust
        )

        print(f"[*] Running Epoch {epoch + 1} Validation...")
        test_loss, test_acc = validate(
            state.params, model_cls, testloader, classification=classification
        )

        print(f"\n=>> Epoch {epoch + 1} Metrics ===")
        print(
            f"\tTrain Loss: {train_loss:.5f} -- Train Accuracy:"
            f" {train_acc:.4f}\n\t Test Loss: {test_loss:.5f} --  Test"
            f" Accuracy: {test_acc:.4f}"
        )

        # Save a checkpoint each epoch & handle best (test loss... not "copacetic" but ehh)
        if train.checkpoint:
            suf = f"-{train.suffix}" if train.suffix is not None else ""
            run_id = f"checkpoints/{dataset}/{layer}-d_model={model.d_model}-lr={train.lr}-bsz={train.bsz}{suf}"
            ckpt_path = checkpoints.save_checkpoint(
                run_id,
                state,
                epoch,
                keep=train.epochs,
            )

        if train.sample is not None:
            if dataset == "mnist":  # Should work for QuickDraw too but untested
                sample_fn = partial(
                    sample_image_prefix, imshape=(28, 28)
                )  # params=state["params"], length=784, bsz=64, prefix=train.sample)
            else:
                raise NotImplementedError(
                    "Sampling currently only supported for MNIST"
                )

            # model_cls = partial(
            #     BatchStackedModel,
            #     layer_cls=layer_cls,
            #     d_output=n_classes,
            #     classification=classification,
            #     **model,
            # )
            samples, examples = sample_fn(
                # run_id,
                params=state.params,
                model=model_cls(decode=True, training=False),
                rng=rng,
                dataloader=testloader,
                prefix=train.sample,
                n_batches=1,
                save=False,
            )
            if wandb is not None:
                samples = [wandb.Image(sample) for sample in samples]
                wandb.log({"samples": samples}, commit=False)
                examples = [wandb.Image(example) for example in examples]
                wandb.log({"examples": examples}, commit=False)

        if (classification and test_acc > best_acc) or (
            not classification and test_loss < best_loss
        ):
            # Create new "best-{step}.ckpt and remove old one
            if train.checkpoint:
                shutil.copy(ckpt_path, f"{run_id}/best_{epoch}")
                if os.path.exists(f"{run_id}/best_{best_epoch}"):
                    os.remove(f"{run_id}/best_{best_epoch}")

            best_loss, best_acc, best_epoch = test_loss, test_acc, epoch

        # Print best accuracy & loss so far...
        print(
            f"\tBest Test Loss: {best_loss:.5f} -- Best Test Accuracy:"
            f" {best_acc:.4f} at Epoch {best_epoch + 1}\n"
        )

        if wandb is not None:
            wandb.log(
                {
                    "train/loss": train_loss,
                    "train/accuracy": train_acc,
                    "test/loss": test_loss,
                    "test/accuracy": test_acc,
                },
                step=epoch,
            )
            wandb.run.summary["Best Test Loss"] = best_loss
            
            wandb.run.summary["Best Test Accuracy"] = best_acc
            wandb.run.summary["Best Epoch"] = best_epoch
    torch.save(model, "mnist_model")
    return state.params, model_cls, testloader

"""
def validate(params, model, testloader, classification=False):
    # Compute average loss & accuracy
    model = model(training=False)
    losses, accuracies = [], []
    for batch_idx, (inputs, labels) in enumerate(tqdm(testloader)):
        inputs = np.array(inputs.numpy())
        labels = np.array(labels.numpy())  # Not the most efficient...
        loss, acc = eval_step(
            inputs, labels, params, model, classification=classification
        )
        losses.append(loss)
        accuracies.append(acc)

    return np.mean(np.array(losses)), np.mean(np.array(accuracies))
"""

def grad_X(params, model, X, y):
    def loss_f(X):
        logits = model.apply({"params": params}, X)
        loss = np.mean(cross_entropy_loss(logits, y))
        return loss
    grad_function = jax.grad(loss_f)
    gradient = grad_function(X)
    return gradient

def fgsmAttackTestJax(params, model, testloader):
    model = model(training=False)
    epss = [0.0, 0.1, 0.2, 0.3, 0.4]
    #epss = [0.4]
    for eps in epss:
        num_correct = 0
        num_samples = 0
        for batch_idx, (inputs, labels) in enumerate(tqdm(testloader)):
            inputs = np.array(inputs.numpy())
            y = np.array(labels.numpy())
            X = copy.deepcopy(inputs)
            gradient = grad_X(params, model, X, y)
            X = X + eps * np.sign(gradient)
            logits = model.apply({"params": params}, X)
            correct = compute_accuracy(logits, y)
            num_correct += np.sum(correct)
            num_samples += len(y)
        accuracy = float(num_correct) / num_samples * 100
        print('\nAttack using FGSM with eps = %.3f, accuracy = %.2f%%' % (eps, accuracy))


def pgdAttackTestJax(params, model, testloader):
    model = model(training=False)
    epss = [0.0, 0.1, 0.2, 0.3, 0.4]
    #epss = [0.4]
    MaxIter = 20
    step_size = 1e-2
    for eps in epss:
        num_correct = 0
        num_samples = 0
        for batch_idx, (inputs, labels) in enumerate(tqdm(testloader)):
            inputs = np.array(inputs.numpy())
            y = np.array(labels.numpy())
            X_original = copy.deepcopy(inputs)
            X = copy.deepcopy(inputs)
            for i in range(MaxIter):
                gradient = grad_X(params, model, X, y)
                X = X + step_size * np.sign(gradient)
                X = X_original + (X-X_original).clip(min=-eps, max=eps)
                X = X.clip(min=-1, max=0)
            logits = model.apply({"params": params}, X)
            #loss = np.mean(cross_entropy_loss(logits, y_))
            correct = compute_accuracy(logits, y)
            num_correct += np.sum(correct)
            num_samples += len(y)
        accuracy = float(num_correct) / num_samples * 100
        print('\nAttack using PGD with eps = %.3f, accuracy = %.2f%%' % (eps, accuracy))

"""
def pgdAttackTestJaxASD():
    model.eval()
    epss = [0.0, 0.1, 0.2, 0.3, 0.4]
	MaxIter = 40
	step_size = 1e-2
	for eps in epss:
		num_correct = 0
		num_samples = 0
		for X_, y_ in loader_test:
			X = Variable(X_.type(dtype), requires_grad=True)
			X_original = Variable(X_.type(dtype), requires_grad=False)
			y = Variable(y_.type(dtype), requires_grad=False).long()
			for i in range(MaxIter):
				logits = model(X)
				loss = F.cross_entropy(logits, y)
				loss.backward()
				with torch.no_grad():
					X.data = X.data + step_size * X.grad.sign() 
					X.data = X_original + (X.data - X_original).clamp(min=-eps, max=eps)
					X.data = X.data.clamp(min=0, max=1)
					X.grad.zero_()
			X.requires_grad = False
			X = (X * 255).long().float() / 255
			logits = model(X)
			_, preds = logits.max(1)
			num_correct += (preds == y).sum()
			num_samples += preds.size(0)
		accuracy = float(num_correct) / num_samples * 100
		print('\nAttack using PGD with eps = %.3f, accuracy = %.2f%%' % (eps, accuracy))
"""
"""

@partial(jax.jit, static_argnums=(4, 5))
def train_step(
    state, rng, batch_inputs, batch_labels, model, classification=False
):
    def loss_fn(params):
        logits, mod_vars = model.apply(
            {"params": params},
            batch_inputs,
            rngs={"dropout": rng},
            mutable=["intermediates"],
        )
        loss = np.mean(cross_entropy_loss(logits, batch_labels))
        acc = np.mean(compute_accuracy(logits, batch_labels))
        return loss, (logits, acc)

    if not classification:
        batch_labels = batch_inputs[:, :, 0]

    grad_fn = jax.value_and_grad(loss_fn, has_aux=True)
    (loss, (logits, acc)), grads = grad_fn(state.params)
    state = state.apply_gradients(grads=grads)
    return state, loss, acc
"""
@hydra.main(version_base=None, config_path="", config_name="config")
def main(cfg: DictConfig) -> None:
    print(OmegaConf.to_yaml(cfg))
    OmegaConf.set_struct(cfg, False)  # Allow writing keys

    # Track with wandb
    if wandb is not None:
        wandb_cfg = cfg.pop("wandb")
        wandb.init(
            **wandb_cfg, config=OmegaConf.to_container(cfg, resolve=True)
        )

    #params, model, testloader = example_train(robust=False, **cfg)
    #pgdAttackTestJax(params, model, testloader)
    #fgsmAttackTestJax(params, model, testloader)

    params, model, testloader = example_train(robust=True, **cfg)
    pgdAttackTestJax(params, model, testloader)
    fgsmAttackTestJax(params, model, testloader)
if __name__ == "__main__":
    main()
