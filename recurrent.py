from flax import linen as nn
import jax
import jax.numpy as np
from jax import lax

class RecurrentModel(nn.Module):
    d_model: int
    N: int
    l_max: int
    decode: bool = False

    @nn.compact
    def __call__(self, xs):
        _RNN = nn.scan(
            nn.GRUCell,
            variable_broadcast="params",
            split_rngs={"params": False},
        )

        layer = nn.GRUCell(self.N)
        dummy_rng = jax.random.PRNGKey(0)
        init_h = layer.initialize_carry(
            dummy_rng, (xs.shape[0],)
        )

        rnn_output = _RNN(features=self.N, name="rnn_cell")(init_h, xs)[1]

        return rnn_output
