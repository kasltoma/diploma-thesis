import copy
from functools import partial
import jax
import jax.numpy as np
import numpy
import torch
from tqdm import tqdm

import foolbox
from foolbox.attacks import SpatialAttack
from foolbox import JAXModel, accuracy, samples

from utilities import *


# a helper function for the gradiend computation
@partial(jax.jit, static_argnums=(1,))
def grad_X(params, model, X, y):
    def loss_f(X):
        logits = model.apply({"params": params}, X)
        loss = np.mean(cross_entropy_loss(logits, y))
        return loss
    grad_function = jax.grad(loss_f)
    gradient = grad_function(X)
    return gradient

# implementation of the FGSM method with an accuracy test
def fgsmAttackTest(params, model, testloader, show_image=False):
    model = model(training=False)
    epss = [0.1, 0.3]
    for eps in epss:
        num_correct = 0
        num_samples = 0
        for batch_idx, (inputs, labels) in enumerate(tqdm(testloader)):
            inputs = np.array(inputs.numpy())
            y = np.array(labels.numpy())
            # compute the adversarial samples
            X = copy.deepcopy(inputs)
            gradient = grad_X(params, model, X, y)
            X_original = X.copy()
            X = X + eps * np.sign(gradient)
            if show_image:
                plot_images_trio(X_original[0], X[0], 28, 28, 3)
                return
            # validate the prediction
            logits = model.apply({"params": params}, X)
            correct = compute_accuracy(logits, y)
            num_correct += np.sum(correct)
            num_samples += len(y)
        accuracy = float(num_correct) / num_samples * 100
        print('\nAttack using FGSM with eps = %.3f, accuracy = %.2f%%' % (eps, accuracy))

# implementation of the PGD attack method with an accuracy test
def pgdAttackTest(params, model, testloader, X_bounds, max_iter=40, step_size=1e-2, show_image=False):
    model = model(training=False)
    epss = [0.1, 0.3]
    for eps in epss:
        num_correct = 0
        num_samples = 0
        for _, (inputs, labels) in enumerate(tqdm(testloader)):
            inputs = np.array(inputs.numpy())
            y = np.array(labels.numpy())
            X_original = inputs
            # compute the adversarial samples
            X = copy.deepcopy(inputs)
            for i in range(max_iter):
                gradient = grad_X(params, model, X, y)
                X = X + step_size * np.sign(gradient)
                X = X_original + (X-X_original).clip(min=-eps, max=eps)
                X = X.clip(min=X_bounds[0], max=X_bounds[1])
            if show_image:
                plot_images_trio(X_original[0], X[0], 28, 28, 1)
                return
            # validate the predictions
            logits = model.apply({"params": params}, X)
            correct = compute_accuracy(logits, y)
            num_correct += np.sum(correct)
            num_samples += len(y)
        accuracy = float(num_correct) / num_samples * 100
        print('\nAttack using PGD with eps = %.3f, accuracy = %.2f%%' % (eps, accuracy))

# a helper function given to the Foolbox interface
@partial(jax.jit, static_argnums=(1,))
def predict_partial(params, model, X):
    logits = model.apply({"params": params}, X)
    prediction = logits
    return prediction

# implementation of the Foolbox-provided attacks with an accuracy test
def foolboxAttackTest(params, model, testloader, X_bounds, foolbox_attack:callable, attack_params={}, serial_data=True, show_image=False):
    model = model(training=False)
    num_correct = 0
    num_samples = 0
    def predict(X):
        #reshape to a series for classification
        N, C = X.shape[0], X.shape[1]
        X = X.reshape(N, C, -1)
        if serial_data:
            X = X.swapaxes(1,2)
        X = np.array(X.numpy())
        logits = predict_partial(params, model, X)
        prediction = torch.from_numpy( numpy.asarray(logits) )
        return prediction
    fmodel = JAXModel(predict, X_bounds)
    attack = foolbox_attack(grid_search=True, **attack_params)
    for batch_idx, (inputs, labels) in enumerate(tqdm(testloader)):
        #reshape to 2d for transformations
        if serial_data:
            inputs = inputs.swapaxes(1, 2)
        N, C = inputs.shape[0], inputs.shape[1]
        W = H = int(numpy.sqrt(inputs.shape[-1])+0.5)
        X = inputs.reshape(N, C, W, H)
        y = np.array(labels.numpy())
        # generate the adversarial samples
        criterion = foolbox.criteria.Misclassification(labels)
        X_adversarial, _, restored = attack(fmodel, X, criterion=criterion)
        X_adversarial = X_adversarial.reshape(N, C, -1)
        if serial_data:
            X_classif = np.array( X_adversarial.swapaxes(1,2).numpy() ) 
        else:
            X_classif = np.array( X_adversarial.numpy() )
        # validate the prediction
        logits = model.apply({"params": params}, X_classif)
        correct = compute_accuracy(logits, y)
        num_correct += np.sum(correct)
        num_samples += len(y)
        if show_image and int(correct[0]) == 0:
            if y[0] != 10:
                continue
            X_ = np.array(X.reshape(N, C, -1).swapaxes(1, 2).numpy())
            X_adversarial_ = np.array(X_adversarial.swapaxes(1, 2).numpy())
            print("classified as :", np.argmax(logits[0]))
            plot_images_trio(X_[0], X_adversarial_[0], W, H, C)
            return
    accuracy = float(num_correct) / num_samples * 100
    print("Accuracy on adversarial examples:", accuracy)

def spatialAttackTest(params, model, testloader, X_bounds, serial_data=True, show_image=False):
    foolboxAttackTest(params, model, testloader, X_bounds, SpatialAttack, serial_data=serial_data, show_image=show_image)
