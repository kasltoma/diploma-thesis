import hydra
from omegaconf import DictConfig, OmegaConf
import wandb

from data import *
from lstm import LSTMRecurrentModel
from s4 import BatchStackedModel, S4Layer, SSMLayer
from train import *
from utilities import *

@hydra.main(version_base=None, config_path="", config_name="config")
def main(cfg: DictConfig) -> None:
    print(OmegaConf.to_yaml(cfg))
    OmegaConf.set_struct(cfg, False)  # Allow writing keys

    # Track with wandb
    if wandb is not None:
        wandb_cfg = cfg.pop("wandb")
        wandb.init(
            **wandb_cfg, config=OmegaConf.to_container(cfg, resolve=True)
        )

    create_dataset_fn = Datasets[cfg.dataset]

    model_types = ["s4"]
    for model_type in model_types:
        trainloader, testloader, n_classes, l_max, d_input, X_bounds = create_dataset_fn(
            bsz=cfg.train.bsz, series=True, regularization=False
        )
        cfg.model.dropout = 0.0
        cfg.layer = model_type
        model_cls = generate_model_cls(model_type, n_classes, l_max, True, cfg.model)
        
        # non-robust non-reg
        params = example_train(robust=False, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
        save_model_params(params, "saves/mnist_nonrobust"+model_type)
        pgdAttackTest(params, model_cls, testloader, X_bounds)
        fgsmAttackTest(params, model_cls, testloader)
        spatialAttackTest(params, model_cls, testloader, X_bounds)

        # robust non-reg
        params = example_train(robust=True, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
        save_model_params(params, "saves/mnist_robust"+model_type)
        pgdAttackTest(params, model_cls, testloader, X_bounds)
        fgsmAttackTest(params, model_cls, testloader)
        spatialAttackTest(params, model_cls, testloader, X_bounds, serial_data=True)


        print("regularization enabled")
        trainloader, testloader, n_classes, l_max, d_input, X_bounds = create_dataset_fn(
            bsz=cfg.train.bsz, series=True, regularization=True
        )
        cfg.model.dropout = 0.5
        model_cls = generate_model_cls(model_type, n_classes, l_max, True, cfg.model)
        # non-robust reg
        params = example_train(robust=False, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
        save_model_params(params, "saves/mnist_nonrobust_reg"+model_type)
        pgdAttackTest(params, model_cls, testloader, X_bounds)
        fgsmAttackTest(params, model_cls, testloader)
        spatialAttackTest(params, model_cls, testloader, X_bounds)

        # robust reg
        params = example_train(robust=True, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
        save_model_params(params, "saves/mnist_robust_reg"+model_type)
        pgdAttackTest(params, model_cls, testloader, X_bounds)
        fgsmAttackTest(params, model_cls, testloader)
        spatialAttackTest(params, model_cls, testloader, X_bounds, serial_data=True)

    model_type = "ff" # for the ff neural network, we need to get a different data loader (flattened, not data series)
    trainloader, testloader, n_classes, l_max, d_input, X_bounds = create_dataset_fn(
        bsz=cfg.train.bsz, series=False, regularization=False
    )

    cfg.layer = model_type
    model_cls = generate_model_cls(model_type, n_classes, l_max, True, cfg.model)
        
    # non-robust non-reg
    params = example_train(robust=False, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
    save_model_params(params, "saves/mnist_nonrobust"+model_type)
    pgdAttackTest(params, model_cls, testloader, X_bounds)
    fgsmAttackTest(params, model_cls, testloader)
    spatialAttackTest(params, model_cls, testloader, X_bounds)

    # robust non-reg
    params = example_train(robust=True, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
    save_model_params(params, "saves/mnist_robust"+model_type)
    pgdAttackTest(params, model_cls, testloader, X_bounds)
    fgsmAttackTest(params, model_cls, testloader)
    spatialAttackTest(params, model_cls, testloader, X_bounds, serial_data=True)

    print("regularization enabled")
    model_cls = generate_model_cls(model_type, n_classes, l_max, True, cfg.model)
    cfg.model.dropout = 0.5
    trainloader, testloader, n_classes, l_max, d_input, X_bounds = create_dataset_fn(
        bsz=cfg.train.bsz, series=False, regularization=True
    )
    # non-robust reg
    params = example_train(robust=False, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
    save_model_params(params, "saves/mnist_nonrobust_reg"+model_type)
    pgdAttackTest(params, model_cls, testloader, X_bounds)
    fgsmAttackTest(params, model_cls, testloader)
    spatialAttackTest(params, model_cls, testloader, X_bounds)

    params = example_train(robust=True, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
    # robust reg
    save_model_params(params, "saves/mnist_robust_reg"+model_type)
    pgdAttackTest(params, model_cls, testloader, X_bounds)
    fgsmAttackTest(params, model_cls, testloader)
    spatialAttackTest(params, model_cls, testloader, X_bounds, serial_data=False)


if __name__ == "__main__":
    main()