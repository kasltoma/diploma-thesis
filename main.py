import hydra
from omegaconf import DictConfig, OmegaConf
import wandb

from data import *
from lstm import LSTMRecurrentModel
from s4 import BatchStackedModel, S4Layer, SSMLayer
from train import *
from utilities import *

@hydra.main(version_base=None, config_path="", config_name="config")
def main(cfg: DictConfig) -> None:
    # get the config file
    print(OmegaConf.to_yaml(cfg))
    OmegaConf.set_struct(cfg, False)  # Allow writing keys

    # Track with wandb
    if wandb is not None:
        wandb_cfg = cfg.pop("wandb")
        wandb.init(
            **wandb_cfg, config=OmegaConf.to_container(cfg, resolve=True)
        )

    # load the dataset
    create_dataset_fn = Datasets[cfg.dataset]
    trainloader, testloader, n_classes, l_max, d_input, X_bounds = create_dataset_fn(
        bsz=cfg.train.bsz, series=True, regularization=False
    )

    # generate the model
    model_cls = generate_model_cls(cfg.layer, n_classes, l_max, True, cfg.model)
    
    # the robust training, learn the NNs weights
    params = example_train(robust=False, X_bounds=X_bounds, model_cls=model_cls, trainloader=trainloader, testloader=testloader, **cfg)
    save_model_params(params, "saves/mnist_nonrobust"+cfg.layer)
    # validate resistance towards the attack methods
    pgdAttackTest(params, model_cls, testloader, X_bounds)
    fgsmAttackTest(params, model_cls, testloader)

if __name__ == "__main__":
    main()