import os
import numpy as np
import pandas as pd
import torch
import torchvision
import torchvision.transforms as transforms
import torchaudio.transforms as audio_transforms
from torchaudio.datasets import SPEECHCOMMANDS
from torchvision.transforms import v2
from torch.utils.data import Dataset
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import random_split
import wave

class AddRandomNoise(object):
    def __init__(self, noise_level):
        self.noise_level = noise_level

    def __call__(self, tensor):
        noise = torch.randn_like(tensor) * self.noise_level
        noisy_tensor = tensor + noise
        return noisy_tensor

# ### MNIST Classification
# **Task**: Predict MNIST class given sequence model over pixels (784 pixels => 10 classes).
def create_mnist_classification_dataset(bsz=128, series=True, regularization=False):
    print("[*] Generating MNIST Classification Dataset...")

    # Constants
    SEQ_LENGTH, N_CLASSES, IN_DIM = 784, 10, 1
    if not series:
        SEQ_LENGTH, IN_DIM = IN_DIM, SEQ_LENGTH
    X_BOUNDS = (-1, 1)
    reg_tf = transforms.Compose(
        [
            transforms.ToTensor(),
            #v2.RandomResize(32, 36),
            #v2.RandomCrop(28),
            v2.RandomRotation(30),
            transforms.Normalize(mean=0.5, std=0.5),
            AddRandomNoise(0.1),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )
    tf = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Normalize(mean=0.5, std=0.5),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )
    if regularization:
        train_tf = reg_tf
    else:
        train_tf = tf

    train = torchvision.datasets.MNIST(
        "./data", train=True, download=True, transform=train_tf
    )
    test = torchvision.datasets.MNIST(
        "./data", train=False, download=True, transform=tf
    )

    # Return data loaders, with the provided batch size
    trainloader = torch.utils.data.DataLoader(
        train, batch_size=bsz, shuffle=True
    )
    testloader = torch.utils.data.DataLoader(
        test, batch_size=bsz, shuffle=False
    )

    return trainloader, testloader, N_CLASSES, SEQ_LENGTH, IN_DIM, X_BOUNDS

# ### GTSRB Classification
# **Task**: Predict road sign class given sequence model over pixels (3*1024 pixels => 10 classes).
def create_gtsrb_classification_dataset(bsz=128, series=True, regularization=False):
    print("[*] Generating GTSRB (German Traffic Sign Recognition Benchmark) Classification Dataset...")

    # Constants
    SEQ_LENGTH, N_CLASSES, IN_DIM = 1024, 43, 3
    if not series:
        SEQ_LENGTH, IN_DIM = IN_DIM, SEQ_LENGTH
    X_BOUNDS = (-1, 1)
    reg_tf = transforms.Compose(
        [
            transforms.ToTensor(),
            #v2.RandomResize(32, 36),
            #v2.RandomCrop(28),
            transforms.Resize(32, 32),
            v2.RandomRotation(30),
            transforms.Normalize(mean=0.5, std=0.5),
            AddRandomNoise(0.1),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )
    tf = transforms.Compose(
        [
            transforms.Resize(32, 32),
            transforms.ToTensor(),
            transforms.Normalize(mean=0.5, std=0.5),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )
    if regularization:
        train_tf = reg_tf
    else:
        train_tf = tf

    train_dataset = torchvision.datasets.GTSRB(root='./data', split="train", transform=train_tf, download=True)
    test_dataset = torchvision.datasets.GTSRB(root='./data', split="test", transform=tf, download=True)
    
    # Return data loaders, with the provided batch size
    trainloader = torch.utils.data.DataLoader(
        train_dataset, batch_size=bsz, shuffle=True
    )
    testloader = torch.utils.data.DataLoader(
        test_dataset, batch_size=bsz, shuffle=False
    )

    return trainloader, testloader, N_CLASSES, SEQ_LENGTH, IN_DIM, X_BOUNDS

# ### CIFAR-10 Classification
# **Task**: Predict CIFAR-10 class given sequence model over pixels (32 x 32 x 3 RGB image => 10 classes).
def create_cifar_classification_dataset(bsz=128):
    print("[*] Generating CIFAR-10 Classification Dataset")

    # Constants
    SEQ_LENGTH, N_CLASSES, IN_DIM = 32 * 32, 10, 3
    X_BOUNDS = (-1, 1)
    tf = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Normalize(mean=0.5, std=0.5),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )

    train = torchvision.datasets.CIFAR10(
        "./data", train=True, download=True, transform=tf
    )
    test = torchvision.datasets.CIFAR10(
        "./data", train=False, download=True, transform=tf
    )

    # Return data loaders, with the provided batch size
    trainloader = torch.utils.data.DataLoader(
        train, batch_size=bsz, shuffle=True
    )
    testloader = torch.utils.data.DataLoader(
        test, batch_size=bsz, shuffle=False
    )

    return trainloader, testloader, N_CLASSES, SEQ_LENGTH, IN_DIM, X_BOUNDS

# ### IMAGENETTE Classification
# **Task**: Predict MNIST class given sequence model over pixels (784 pixels => 10 classes).
def create_imagenette_classification_dataset(bsz=128):
    print("[*] Generating Imagenette Classification Dataset...")

    # Constants
    SEQ_LENGTH, N_CLASSES, IN_DIM = 224*224, 10, 3
    tf = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Normalize(mean=0.5, std=0.5),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )
    dataset = torchvision.datasets.Imagenette(root='./data', transform=tf, download=True)

    train_size = int(0.8 * len(dataset))
    test_size = len(dataset) - train_size
    train_dataset, test_dataset = random_split(dataset, [train_size, test_size])
    # Return data loaders, with the provided batch size
    trainloader = torch.utils.data.DataLoader(
        train_dataset, batch_size=bsz, shuffle=True
    )
    testloader = torch.utils.data.DataLoader(
        test_dataset, batch_size=bsz, shuffle=False
    )
    return trainloader, testloader, N_CLASSES, SEQ_LENGTH, IN_DIM


def ensure_size(sample, soundlen):
    if len(sample) < soundlen:
        zeros_to_append = soundlen - len(sample)
        sample = np.pad(sample, (0, zeros_to_append), mode='constant')
    sample = sample[:soundlen]
    return sample    

class CustomDataset(Dataset):
    def __init__(self, X, y, transform=None, as_int=False):
        self.X = X
        self.y = y
        self.transform = transform
        self.as_int = as_int
        
    def __len__(self):
        return len(self.X)
    
    def __getitem__(self, idx):
        sample = {'feature': self.X[idx], 'label': self.y[idx]}
        sample, label = self.X[idx], self.y[idx]
        
        if self.transform is not None:
            sample = self.transform(sample)
        if self.as_int:
            sample = sample.int() 
        return sample, label


def wav_to_array(file_path):
    # Open the .wav file
    with wave.open(file_path, 'rb') as wav_file:
        # Get the number of frames (samples)
        num_frames = wav_file.getnframes()
        # Read the audio data as a string of bytes
        audio_data = wav_file.readframes(num_frames)
        # Convert the audio data to a numpy array
        audio_array = np.frombuffer(audio_data, dtype=np.int16)
    return audio_array

def normalize(features):
    features = np.array(features)
    f_min = np.min(features)
    f_max = np.max(features)
    features = (features-f_min) / (f_max - f_min)
    features = 2*features - 1
    return features

data_path = "/data/UrbanSound8K/audio"
metadata_path = "/data/UrbanSound8K/metadata/UrbanSound8K.csv"
def create_sound_samples_classification_dataset(bsz=4, series=True, soundlen=40_000, regularization=False, binary=True):
    features = []
    labels = []
    metadata = pd.read_csv(metadata_path)

    for index, row in metadata.iterrows():
        file_path = os.path.join(data_path, f"fold{row['fold']}", f"{row['slice_file_name']}")
        try:
            array = wav_to_array(file_path)
        except:
            continue
        trimmed_array = ensure_size(array, soundlen)
        label = row['class']
        if binary:
            if label in ["siren", "dog_bark", "gun_shot"]:
                label = "dangerous"
            else:
                label = "benign"
        labels.append(label)
        features.append(trimmed_array)
    
    print("sounds loaded: ", len(labels))
    features = [ np.array([x.astype(np.float32)]).T for x in features ]
    features = normalize(features)
    X_bounds = (np.min(features), np.max(features))

    le = LabelEncoder()
    labels = le.fit_transform(labels)

    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=42)
    X_train = [ np.array([x.astype(np.float32)]).T for x in X_train ]
    X_test =  [ np.array([x.astype(np.float32)]).T for x in X_test ]
    SEQ_LENGTH = soundlen
    IN_DIM = 1
    if not series:
        SEQ_LENGTH, IN_DIM = IN_DIM, SEQ_LENGTH
    reg_tf = transforms.Compose(
        [
            transforms.ToTensor(),
            audio_transforms.TimeMasking(time_mask_param=soundlen//5),
            AddRandomNoise(0.1),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )
    tf = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )

    if regularization:
        train_tf = reg_tf
    else:
        train_tf = tf
    
    if binary:
        n_classes = 2
    else:
        n_classes = 10
    
    train_dataset = CustomDataset(X_train, y_train, transform=train_tf)
    test_dataset = CustomDataset(X_test, y_test, transform=tf)
    trainloader = torch.utils.data.DataLoader(
        train_dataset, batch_size=bsz, shuffle=True
    )
    testloader = torch.utils.data.DataLoader(
        test_dataset, batch_size=bsz, shuffle=False
    )
    return trainloader, testloader, n_classes, SEQ_LENGTH, IN_DIM, X_bounds

def create_sound_samples_classification_dataset_spectrogram(bsz=4, series=True, soundlen=40_000, regularization=False, binary=True):
    features = []
    labels = []
    metadata = pd.read_csv(metadata_path)

    for index, row in metadata.iterrows():
        file_path = os.path.join(data_path, f"fold{row['fold']}", f"{row['slice_file_name']}")
        try:
            array = wav_to_array(file_path)
        except:
            continue
        trimmed_array = ensure_size(array, soundlen)
        label = row['class']
        if binary:
            if label in ["siren", "dog_bark", "gun_shot"]:
                label = "dangerous"
            else:
                label = "benign"
        labels.append(label)
        features.append(trimmed_array)
    
    print("sounds loaded: ", len(labels))
    features = [ np.array([x.astype(np.float32)]).T for x in features ]
    features = normalize(features)
    X_bounds = (np.min(features), np.max(features))

    le = LabelEncoder()
    labels = le.fit_transform(labels)

    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=42)
    X_train = [ np.array([x.astype(np.float32)]).T for x in X_train ]
    X_test =  [ np.array([x.astype(np.float32)]).T for x in X_test ]
    SG = SpectroGram(n_mels=80, sample_rate=16000)
    test_sample = SG.__call__(X_train[0])
    shp = test_sample.shape
    IN_DIM, SEQ_LENGTH = shp[0], shp[1]
    if not series:
        IN_DIM = test_sample.numel()
        SEQ_LENGTH = 1
    reg_tf = transforms.Compose(
        [
            transforms.ToTensor(),
            #audio_transforms.TimeMasking(time_mask_param=soundlen//5),
            #AddRandomNoise(0.1),
            SpectroGram(80, 16000),
            AddRandomNoise(0.1),
            transforms.Lambda(lambda x: x.reshape((1, IN_DIM, SEQ_LENGTH))),
            transforms.Normalize(mean=0.5, std=0.5),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )
    tf = transforms.Compose(
        [
            transforms.ToTensor(),
            SpectroGram(80, 16000),
            transforms.Lambda(lambda x: x.reshape((1, IN_DIM, SEQ_LENGTH))),
            transforms.Normalize(mean=0.5, std=0.5),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )

    if regularization:
        train_tf = reg_tf
    else:
        train_tf = tf
    
    if binary:
        n_classes = 2
    else:
        n_classes = 10
    
    train_dataset = CustomDataset(X_train, y_train, transform=train_tf)
    test_dataset = CustomDataset(X_test, y_test, transform=tf)
    trainloader = torch.utils.data.DataLoader(
        train_dataset, batch_size=bsz, shuffle=True
    )
    testloader = torch.utils.data.DataLoader(
        test_dataset, batch_size=bsz, shuffle=False
    )
    return trainloader, testloader, n_classes, SEQ_LENGTH, IN_DIM, X_bounds

def gather_dataset(dataset, classes_to_keep, len_limit=16000, sample_limit=1000):
    X, Y = [], []
    for i, dat in enumerate(dataset):
        label = dat[2]
        if classes_to_keep is not None and label not in classes_to_keep:
            continue
        wave = dat[0].reshape(-1)
        wave = ensure_size(wave, len_limit )
        X.append( wave.reshape((-1,1)) )
        Y.append(label)
        if len(X) > sample_limit:
            break
    X = np.array(X)
    le = LabelEncoder()
    Y = le.fit_transform(Y)
    Y = np.array(Y)
    return X, Y

class SubsetSC(SPEECHCOMMANDS):
    def __init__(self, subset: str = None):
        super().__init__("./data/", download=True)

        def load_list(filename):
            filepath = os.path.join(self._path, filename)
            with open(filepath) as fileobj:
                return [os.path.normpath(os.path.join(self._path, line.strip())) for line in fileobj]

        if subset == "testing":
            self._walker = load_list("testing_list.txt")
        elif subset == "training":
            excludes = load_list("validation_list.txt") + load_list("testing_list.txt")
            excludes = set(excludes)
            self._walker = [w for w in self._walker if w not in excludes]

class CommandsDataset(Dataset):
    def __init__(self, X, y, transform=None):
        self.X = X
        self.y = y
        self.transform = transform
        
    def __len__(self):
        return len(self.X)
    
    def __getitem__(self, idx):
        sample, label = self.X[idx], self.y[idx]
        
        if self.transform is not None:
            sample = self.transform(sample)
        return sample, label
    
class Conv1DTransform:
    def __init__(self, kernel, stride=3):
        self.kernel = kernel
        self.stride = stride

    def __call__(self, sample):
        sample = torch.tensor(sample).unsqueeze(0).float()  # Convert to tensor and add batch dimension
        kernel = self.kernel.unsqueeze(0).unsqueeze(0).float()  # Reshape kernel for convolution
        sample = sample.swapaxes(1,2)
        convolved_sample = torch.conv1d(sample, kernel, padding=self.kernel.size(0)//2, stride=self.stride)  # Perform 1D convolution
        convolved_sample = convolved_sample.swapaxes(1,2).squeeze(0)
        return convolved_sample 

def create_speech_command_classification(bsz=4, series=True, soundlen=16_000, regularization=False, 
                                         labels=["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]):
    train_set = SubsetSC("training")
    test_set = SubsetSC("testing")
    train_X, train_Y = gather_dataset(train_set, classes_to_keep=labels, len_limit=soundlen, sample_limit=108000)
    train_X = normalize(train_X)
    test_X, test_Y = gather_dataset(test_set, classes_to_keep=labels, len_limit=soundlen, sample_limit=20000)
    test_X = normalize(test_X)
    X_bounds = (np.min(train_X), np.max(train_X))
    convolve_kernel = torch.tensor([0.1, 0.3, 0.7, 0.3, 0.1])
    if series:
        reg_tf = transforms.Compose(
            [
                transforms.ToTensor(),
                audio_transforms.TimeMasking(time_mask_param=soundlen//5),
                AddRandomNoise(0.1),
                transforms.Lambda(lambda x: x.view(1, -1).t()),
                Conv1DTransform(convolve_kernel),
            ]
        )
        tf = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Lambda(lambda x: x.view(1, -1).t()),
                Conv1DTransform(convolve_kernel),
            ]
        )
    else:
        reg_tf = transforms.Compose(
            [
                transforms.ToTensor(),
                audio_transforms.TimeMasking(time_mask_param=soundlen//5),
                AddRandomNoise(0.1),
                transforms.Lambda(lambda x: x.view(1, -1).t()),
                Conv1DTransform(convolve_kernel),
                transforms.Lambda(lambda x: x.t().unsqueeze(0)),
            ]
        )
        tf = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Lambda(lambda x: x.view(1, -1).t()),
                Conv1DTransform(convolve_kernel),
                transforms.Lambda(lambda x: x.t().unsqueeze(0)),
            ]
        )
    if regularization:
        train_tf = reg_tf
    else:
        train_tf = tf
    
    SEQ_LENGTH = (soundlen//3 +1)
    IN_DIM = 32
    if not series:
        SEQ_LENGTH, IN_DIM = IN_DIM, SEQ_LENGTH
    
    train_dataset = CustomDataset(train_X, train_Y, transform=train_tf)
    test_dataset = CustomDataset(test_X, test_Y, transform=tf)

    trainloader = torch.utils.data.DataLoader(
        train_dataset, batch_size=bsz, shuffle=True
    )
    testloader = torch.utils.data.DataLoader(
        test_dataset, batch_size=bsz, shuffle=False
    )

    return trainloader, testloader, len(labels), SEQ_LENGTH, IN_DIM, X_bounds


class SpectroGram:
    def __init__(self, n_mels=64, sample_rate=16000):
        self.n_mels = n_mels
        self.sample_rate = sample_rate

    def __call__(self, sample):
        sample = torch.tensor(sample).reshape(-1)
        n_fft = 1024
        top_db = 80
        # spec has shape [channel, n_mels, time], where channel is mono, stereo etc
        spec_transform = audio_transforms.MelSpectrogram(self.sample_rate, n_fft=n_fft, n_mels=self.n_mels)
        #spec_transform = audio_transforms.MFCC(sr, n_mels=n_mels)
        spec = spec_transform(sample)
        # Convert to decibels
        spec = audio_transforms.AmplitudeToDB(top_db=top_db)(spec)
        return (spec)
    

def create_speech_command_classification_spectrogram(bsz=4, series=True, soundlen=16_000, regularization=False, labels=["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]):
    train_set = SubsetSC("training")
    test_set = SubsetSC("testing")
    train_X, train_Y = gather_dataset(train_set, classes_to_keep=labels, len_limit=soundlen, sample_limit=108000)
    test_X, test_Y = gather_dataset(test_set, classes_to_keep=labels, len_limit=soundlen, sample_limit=20000)
    X_bounds = (-1, 1)
    SG = SpectroGram(n_mels=80, sample_rate=16000)
    test_sample = SG.__call__(train_X[0])
    shp = test_sample.shape
    IN_DIM, SEQ_LENGTH = shp[0], shp[1]
    if not series:
        IN_DIM = test_sample.numel()
        SEQ_LENGTH = 1
    
    reg_tf = transforms.Compose(
        [
            transforms.ToTensor(),
            audio_transforms.TimeMasking(time_mask_param=soundlen//5),
            AddRandomNoise(0.1),
            SpectroGram(80, 16000),
            #AddRandomNoise(0.1),
            transforms.Lambda(lambda x: x.reshape((1, IN_DIM, SEQ_LENGTH))),
            transforms.Normalize(mean=0.5, std=0.5),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )
    tf = transforms.Compose(
        [
            transforms.ToTensor(),
            SpectroGram(80, 16000),
            transforms.Lambda(lambda x: x.reshape((1, IN_DIM, SEQ_LENGTH))),
            transforms.Normalize(mean=0.5, std=0.5),
            transforms.Lambda(lambda x: x.view(IN_DIM, SEQ_LENGTH).t()),
        ]
    )

    if regularization:
        train_tf = reg_tf
    else:
        train_tf = tf
    
    train_dataset = CustomDataset(train_X, train_Y, transform=train_tf)
    test_dataset = CustomDataset(test_X, test_Y, transform=tf)

    trainloader = torch.utils.data.DataLoader(
        train_dataset, batch_size=bsz, shuffle=True
    )
    testloader = torch.utils.data.DataLoader(
        test_dataset, batch_size=bsz, shuffle=False
    )

    return trainloader, testloader, len(labels), SEQ_LENGTH, IN_DIM, X_bounds

Datasets = {
    "mnist-classification": create_mnist_classification_dataset,
    "cifar-classification": create_cifar_classification_dataset,
    "gtsrb-classification": create_gtsrb_classification_dataset,
    "sound-samples-classification": create_sound_samples_classification_dataset,
    "sound-samples-classification-spectrogram": create_sound_samples_classification_dataset_spectrogram,
    "create-speech-command-classification" : create_speech_command_classification,
    "create-speech-command-classification-spectrogram" : create_speech_command_classification_spectrogram,
}
