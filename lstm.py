from flax import linen as nn
import jax

class LSTMRecurrentModel(nn.Module):
    d_model: int
    N: int
    l_max: int
    decode: bool = True

    @nn.compact
    def __call__(self, xs):
        LSTM = nn.scan(
            nn.OptimizedLSTMCell,
            variable_broadcast="params",
            split_rngs={"params": False},
        )

        layer = nn.OptimizedLSTMCell(self.N)
        dummy_rng = jax.random.PRNGKey(0)
        init_h = layer.initialize_carry(
            dummy_rng, (self.d_model,)
        )

        lstm_output = LSTM(features=self.N, name="lstm_cell")(init_h, xs)[1]

        return lstm_output
