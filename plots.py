import matplotlib.pyplot as plt
import jax.numpy as np
import numpy


def plot_learning_accuracy(ys:list[list[int]], labels:list[str], title:str):
    n_epochs = len(ys[0])
    x = [i+1 for i in range(n_epochs)]
    for y, label in zip(ys, labels):
        plt.plot(x, y, marker="o", label=label)
    
    ax = plt.gca()
    ax.set_ylim([0, 1])
    plt.xlabel("epoch")
    plt.ylabel("accurracy on test data")
    plt.title(title)
    plt.legend()
    plt.show()

def plot_learning_accuracy_vs_loss(accs:list[list[int]], losses:list[list[int]], labels:list[str], title:str):
    n_epochs = len(accs[0])
    x = [i+1 for i in range(n_epochs)]
    
    fig, ax1 = plt.subplots()
    
    ax1.set_xlabel('epoch')
    ax1.set_ylabel('accuraccy')#, color='b')
    ax1.set_ylim([0, 1])
    ax2 = ax1.twinx()
    ax2.set_ylabel('loss')#, color='r')

    ax1.plot(x, accs[0], 'b-', marker="o", label=labels[0], color="red")
    ax2.plot(x, losses[0], 'b-', marker="+", label=labels[0], color="lightcoral")

    ax1.plot(x, accs[1], 'b-', marker="o", label=labels[1], color="blue")
    ax2.plot(x, losses[1], 'b-', marker="+", label=labels[1], color="cyan")

    plt.title(title)
    plt.legend()
    plt.show()

def plot_attack_accuracy(Ys:list[list[int]], cols:list[str], labels:list[str], title:str):
    x = numpy.arange(len(cols))
    width = 0.2 
    fig, ax = plt.subplots()
    rects1 = ax.bar(x - 1.5*width, Ys[0], width, label=labels[0])
    rects2 = ax.bar(x - 0.5*width, Ys[1], width, label=labels[1])
    rects3 = ax.bar(x + 0.5*width, Ys[2], width, label=labels[2])
#    rects4 = ax.bar(x + 1.5*width, Ys[3], width, label=labels[3])

#    for rects in [rects1, rects2, rects3, rects4]:
    for rects in [rects1, rects2, rects3]:
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

    ax.set_xlabel('Categories')
    ax.set_ylabel('Accuraccies')
    ax.set_title(title)
    ax.set_xticks(x)
    ax.set_xticklabels(cols)
    ax.legend()
    plt.legend()
    plt.show()

def increase_in_computation_time(nonrobust_times, robust_times, labels):
    markers = ["o", "x", "+", "v", "s"]
    colors = ["blue", "red", "green", "orange"]
    n = len(nonrobust_times)
    for i in range(n):
        for j in range(len(nonrobust_times[i])):
            plt.plot(nonrobust_times[i][j], robust_times[i][j], markers[i], label=labels[i], color=colors[i])
    plt.xlabel("Time, nonrobust")
    plt.ylabel("Time, robust")
    plt.legend()
    #plt.gca().set_aspect('equal')
    plt.title("Relation of required time, robust and nonrobust training")
    #plt.yscale("log")
    plt.show()

def main():
    GRU_default_accuracies_mnist = [0.7812, 0.8331, 0.8707, 0.7362, 0.9234, 0.9361, 0.9557, 0.9565, 0.9648, 0.9633]
    LSTM_default_accuracies_mnist = [0.7823, 0.8008, 0.7829, 0.8623, 0.8645, 0.8705, 0.7073, 0.9012, 0.9346, 0.9370]
    S4_default_accuracies_mnist = [0.8897, 0.9415, 0.9634, 0.9681, 0.9666, 0.9763, 0.9778, 0.9803, 0.9793, 0.9729]
    FF_default_accuracies_mnist = [0.9519, 0.9647, 0.9627, 0.9696, 0.9733, 0.9675, 0.9716, 0.9731, 0.9731, .9718]
    GRU_robust_accuracies_mnist = [0.8386, 0.8388, 0.8821, 0.8908, 0.8975, 0.9028, 0.9087, 0.9053, 0.9202, 0.9159]
    LSTM_robust_accuracties_mnist = [0.9458, 0.9639, 0.9634, 0.9704, 0.9610, 0.9692, 0.9680, 0.9698, 0.9740, 0.9707]
    S4_robust_accuracies_mnist = [0.9134, 0.9500, 0.9676, 0.9757, 0.9746, 0.9786, 0.9741, 0.9802, 0.9824, 0.9788]
    FF_robust_accuracies_mnist = [0.8435, 0.8581, 0.8994, 0.9121, 0.9208, 0.9213, 0.9239, 0.9282, 0.9344, 0.9330]
    # ys = [GRU_default_accuracies_mnist, LSTM_default_accuracies_mnist, S4_default_accuracies_mnist, FF_default_accuracies_mnist]
    # ys = [GRU_robust_accuracies_mnist, LSTM_robust_accuracties_mnist, S4_robust_accuracies_mnist, FF_robust_accuracies_mnist]
    # labels = ["GRU, 133642 params", "LSTM, 166410 params", "S4, 100618 params", "FF, 151562 params (non-serial data)"]
    # title = "Learning on serialized MNIST classification, non-robust"
    # title = "Learning on serialized MNIST classification, robust"
    # plot_learning_accuracy(ys, labels, title)

    S4_gtsrb_nonrobust_accs   = [0.2283, 0.3246, 0.4146, 0.4620, 0.5188, 0.5226, 0.5440, 0.5542, 0.5659, 0.5990, 0.6113, 0.6082, 0.6003, 0.6196, 0.6227, 0.6290, 0.6368, 0.6493, 0.6321, 0.6490]
    S4_gtsrb_robust_accs      = [0.0530, 0.0693, 0.1399, 0.1422, 0.1730, 0.1704, 0.1888, 0.2178, 0.2252, 0.2739, 0.2819, 0.3023, 0.3268, 0.3059, 0.3318, 0.3472, 0.3227, 0.3516, 0.3461, 0.3586]
    FF_gtsrb_nonrobust_accs   = [0.6491, 0.7141, 0.7127, 0.7253, 0.7500, 0.7290, 0.7515, 0.6895, 0.7581, 0.7751, 0.7483, 0.7551, 0.7429, 0.7623, 0.7936, 0.7942, 0.8020, 0.7936, 0.7904, 0.8172]
    FF_gtsrb_robust_accs      = [0.3761, 0.3631, 0.3450, 0.3363, 0.4086, 0.4098, 0.4163, 0.3856, 0.3896, 0.4209, 0.4353, 0.4088, 0.4071, 0.3936, 0.4175, 0.4398, 0.4372, 0.4405, 0.4619, 0.4736]
    ys = [S4_gtsrb_nonrobust_accs, S4_gtsrb_robust_accs, FF_gtsrb_nonrobust_accs, FF_gtsrb_robust_accs]
    labels = ["S4, nonrobust", "S4, Algorithm 1", "FF, nonrobust", "FF, Algorithm 1"]
    title = "Learning on GTSRB dataset"
    #plot_learning_accuracy(ys, labels, title)

    #attacks columns
    cols = ["benign", "PGD eps=0.1", "PGD eps=0.3", "FGSM eps=0.1", "FGSM eps=0.3", "Spatial"]
    labels = ["GRU", "LSTM", "S4", "FF (nonserial)"]
    # GRU_mnist_default_attack_accuraies = [95.7, 36.8, 0.1, 63.2, 8.7, 0]
    # LSTM_mnist_default_attack_accuraies = [91.7, 19.1, 0.9, 23.4, 7.7, 0.0]
    # S4_mnist_default_attack_accuraies = [98.2, 93.3, 34.5, 90.4, 36.2, 5]
    # FF_mnist_default_attack_accuraies = [95.6, 71.3, 3.9, 44.5, 27.0, 95.5]
    # accuraccies = [GRU_mnist_default_attack_accuraies, LSTM_mnist_default_attack_accuraies, S4_mnist_default_attack_accuraies, FF_mnist_default_attack_accuraies]
    # plot_attack_accuracy(accuraccies, cols, labels, "Accuraccies under attacks, MNIST, non-robust")
    # GRU_mnist_robust_attack_accuraies = [95.6, 94.4, 92.0, 94.8, 93.7, 0.0]
    # LSTM_mnist_robust_attack_accuraies = [93.9, 91.0, 86.6, 92.4, 90.5, 0.0]
    # S4_mnist_robust_attack_accuraies = [97.9, 96.5, 91.4, 96.4, 92.2, 2.8]
    # FF_mnist_robust_attack_accuraies = [96.5, 90.7, 54.5, 86.4, 27.0, 96.2]
    # accuraccies = [GRU_mnist_robust_attack_accuraies, LSTM_mnist_robust_attack_accuraies, S4_mnist_robust_attack_accuraies, FF_mnist_robust_attack_accuraies]
    # plot_attack_accuracy(accuraccies, cols, labels, "Accuraccies under attacks, MNIST, robust")

    GRU_plain = [92.1, 47.0, 44.6, 88.7, 85.4, 71.6]
    GRU_alg1  = [21.2, 88.7, 86.4, 14.1, 12.8, 44.6]
    GRU_alg2  = [90.0, 87.4, 85.1, 87.0, 83.6, 86.6]
    accuraccies = [GRU_plain, GRU_alg1, GRU_alg2]
    cols = ["benign", "PGD eps=0.1", "PGD eps=0.3", "FGSM eps=0.1", "FGSM eps=0.3", "Average"]
    labels = ["nonrobust", "Algorithm1", "Algorithm2"]
    #plot_attack_accuracy(accuraccies, cols, labels, "Accuraccies under attacks, SpeechCommands spectrograms, GRU")

    S4_plain = [79.8, 65.1, 43.6, 70.7, 65.5, 64.94]
    S4_alg1 =  [79.6, 71.4, 70.3, 73.4, 67.6, 72.46]
    S4_alg2 =  [78.7, 70.6, 60.5, 71.7, 63.0, 68.9]
    accuraccies = [S4_plain, S4_alg1, S4_alg2]
    cols = ["benign", "PGD eps=0.1", "PGD eps=0.3", "FGSM eps=0.1", "FGSM eps=0.3", "Average"]
    labels = ["nonrobust", "Algorithm1", "Algorithm2"]
    plot_attack_accuracy(accuraccies, cols, labels, "Accuraccies under attacks, SpeechCommands spectrograms, GRU")

    FF_nonrobust   = [70.3, 9.6, 9.1, 68.2, 63.8]
    FF_robust      = [70.5, 11.6, 10.8, 62.8, 59.2]
    GRU_nonrobust  = [92.1, 47.0, 44.6, 88.7, 85.4]
    GRU_robust     = [21.2, 88.7, 86.4, 14.1, 12.8]
    LSTM_nonrobust = [92.0, 50.3, 47.6, 89.7, 87.0]
    LSTM_robust    = [25.6, 87.9, 86.5, 15.3, 13.8]
    S4_nonrobust   = [93.1, 62.2, 59.8, 89.9, 87.0]
    S4_robust      = [21.7, 88.8, 87.6, 18.5, 15.4]
    accuracies = [FF_nonrobust, FF_robust, GRU_nonrobust, GRU_robust]
    accuracies2 = [LSTM_nonrobust, LSTM_robust, S4_nonrobust, S4_robust]
    cols = ["benign", "PGD eps=0.1", "PGD eps=0.3", "FGSM eps=0.1", "FGSM eps=0.3"]
    labels = ["FF nonrobust", "FF robust", "GRU nonrobust", "GRU robust",]
    labels2 = [ "LSTM nonrobust", "LSTM robust", "S4 nonrobust", "S4 robust"]
    #plot_attack_accuracy(accuracies, cols, labels, "Accuraccies under attacks, SpeechCommands spectrograms")
    #plot_attack_accuracy(accuracies2, cols, labels2, "Accuraccies under attacks, SpeechCommands spectrograms")


    GRU_robust_bigstep = [95.6, 94.4, 92.0, 94.8, 93.7]
    GRU_robust_smallstep = [95.6, 94.2, 92.1, 94.6, 93.2]
    GRU_nonrobust = [95.7, 36.8, 0.1, 63.2, 8.7]
    accuraccies = [GRU_nonrobust, GRU_robust_bigstep, GRU_robust_smallstep]
    cols = ["benign", "PGD eps=0.1", "PGD eps=0.3", "FGSM eps=0.1", "FGSM eps=0.3"]
    labels = ["nonrobust", "stepsize=0.15", "stepsize=0.05"]
    #plot_attack_accuracy(accuraccies, cols, labels, "Accuraccies under attacks, MNIST, GRU")

    # S4_dafault_loss_mnist = [0.36987 ,0.19269 ,0.12488 ,0.10527 ,0.10613 ,0.07245 ,0.06648 ,0.06011 ,0.06614 ,0.07944 ]
    # S4_robust_loss_mnist = [1.39622,0.88155,0.52094,0.29728,0.24917,0.19867,0.13763,0.14940,0.10089,0.16241]

    # labels = ["dense, nonrobust", "dense, robust"]
    # ff_nonrobust_acc  = np.array([0.6491,  0.7141,  0.7127,  0.7253,  0.7500,  0.7290,  0.7515,  0.6895,  0.7581,  0.7751,  0.7483,  0.7551,  0.7429,  0.7623,  0.7936,  0.7942,  0.8020,  0.7936,  0.7904,  0.8172])
    # ff_nonrobust_loss = np.array([1.30504, 1.16093, 1.27475, 1.28371, 1.34456, 1.54747, 1.46123, 2.00013, 1.70810, 1.49830, 1.73452, 2.01905, 2.03178, 1.93766, 1.84974, 2.02583, 1.93217, 2.13489, 2.13243, 1.85551])
    # ff_robust_acc     = np.array([0.3761,  0.3631,  0.3450,  0.3363,  0.4086,  0.4098,  0.4163,  0.3856,  0.3896,  0.4209,  0.4353,  0.4088,  0.4071,  0.3936,  0.4175,  0.4398,  0.4372,  0.4405,  0.4619,  0.4736])
    # ff_robust_loss    = np.array([3.57635, 4.50058, 5.15125, 6.37008, 4.06957, 4.33043, 4.22942, 5.30563, 4.22942, 5.10576, 4.22329, 5.23822, 4.43622, 5.28473, 5.60868, 4.69981, 4.21399, 4.58201, 4.14873, 3.88294])
    # accs = [ff_nonrobust_acc, ff_robust_acc]
    # losses = [ff_nonrobust_loss, ff_robust_loss]
    # title = "Training dense NN on GTSRB classification, Loss has lighter colors"
    # plot_learning_accuracy_vs_loss(accs, losses, labels, title)
    
    labels = ["S4, nonrobust", "S4, robust"]
    S4_nonrobust_loss = [0.27478, 0.16715, 0.09831, 0.07803, 0.10473, 0.06762, 0.05637, 0.05581, 0.05331, 0.05426]
    S4_nonrobust_acc  = [0.9192,  0.9470,  0.9704,  0.9760,  0.9656,  0.9809,  0.9825,  0.9809,  0.9828,  0.9819]
    S4_robust_loss    = [0.30391, 0.16338, 0.10467, 0.07837, 0.07797, 0.07022, 0.07789, 0.05735, 0.05520, 0.06671]
    S4_robust_acc     = [0.9134,  0.9500,  0.9676,  0.9757,  0.9746,  0.9786,  0.9741,  0.9802,  0.9824,  0.9788]
    accs = [S4_nonrobust_acc, S4_robust_acc]
    losses = [S4_nonrobust_loss, S4_robust_loss]
    title = "Training S4 NN on MNIST classification, Loss has lighter colors"
    #plot_learning_accuracy_vs_loss(accs, losses, labels, title)

    #LSTM_nonreg_accuracies    = [0.7661,  0.7911,  0.8034,  0.8596,  0.8940,  0.8733,  0.8915,  0.8753,  0.9022,  0.8763,  0.9119,  0.8968,  0.9073,  0.8743,  0.9027,  0.8566]
    #LSTM_nonreg_tr_losses     = [4.95474, 1.42549, 1.14865, 0.89019, 0.76853, 0.66302, 0.56536, 0.55631, 0.44634, 0.44578, 0.40340, 0.40752, 0.36986, 0.36730, 0.38620, 0.36459]
    # LSTM_reg_accuracies       = [0.7905,  0.8499,  0.8824,  0.8620,  0.8966,  0.8820,  0.9090,  0.9087,  0.9181,  0.9090,  0.9185,  0.9266,  0.9119,  0.9327,  0.9221,  0.9186]
    # LSTM_reg_tr_losses        = [4.51501, 1.19583, 0.99025, 0.70551, 0.63605, 0.55889, 0.40549, 0.43469, 0.41017, 0.35068, 0.33762, 0.32218, 0.30967, 0.33819, 0.29843, 0.30641]
    # LSTM_plain_accuracies     = [0.7022,  0.8080,  0.8660,  0.8007,  0.8606,  0.8954,  0.9015,  0.8919,  0.8665,  0.9039,  0.9018,  0.9198,  0.9174,  0.9188,  0.9127,  0.9107]
    # LSTM_plain_tr_losses      = [4.08365, 1.16327, 0.82209, 0.71463, 0.63267, 0.51817, 0.46192, 0.43387, 0.37154, 0.33495, 0.33682, 0.28708, 0.27784, 0.26647, 0.27237, 0.26092]
    # labels = ["LSTM, plain", "LSTM, robust+reg."]
    # accs = [LSTM_plain_accuracies, LSTM_reg_accuracies]
    # losses = [LSTM_plain_tr_losses, LSTM_reg_tr_losses]
    # title = "Training LSTM on Speech Commands dataset, spectrogram+AmpToDB processing, Loss has lighter colors"
    # plot_learning_accuracy_vs_loss(accs, losses, labels, title)

    LSTM_reg_accuracies       = [0.5301,  0.6365,  0.6705,  0.6970,  0.6898,  0.7016,  0.7376,  0.7363,  0.7666,  0.7664,  0.7511,  0.7729,  0.7834,  0.7959,  0.7931,  0.7893]
    LSTM_reg_tr_losses        = [3.88217, 2.35755, 2.07567, 1.76863, 1.70562, 1.96145, 1.57184, 1.48022, 1.35161, 1.30512, 1.22028, 1.20529, 1.24901, 1.17717, 1.08327, 1.08303]
    LSTM_plain_accuracies     = [0.5497,  0.6446,  0.6959,  0.7337,  0.7076,  0.7664,  0.7623,  0.7645,  0.7823,  0.7874,  0.7951,  0.8184,  0.8112,  0.8081,  0.8155,  0.8210]
    LSTM_plain_tr_losses      = [3.72317, 2.15196, 1.77949, 1.63401, 1.52629, 1.71560, 1.47630, 1.29929, 1.17059, 1.14968, 1.01758, 1.06088, 1.05838, 1.00145, 0.91905, 0.93475]
    labels = ["LSTM, plain", "LSTM, robust"]
    accs = [LSTM_plain_accuracies, LSTM_reg_accuracies]
    losses = [LSTM_plain_tr_losses, LSTM_reg_tr_losses]
    title = "Training LSTM on Speech Commands dataset, spectrogram processing, Loss has lighter colors"
    #plot_learning_accuracy_vs_loss(accs, losses, labels, title)


    # TIMES
    labels = ["S4", "FF", "LSTM", "GRU"]
    S4_nonrobust_times       = [75,   41,   47,   45,   46]
    S4_robust_times          = [1400, 650,  1250, 1260, 990]
    FF_nonrobust_times       = [35,   27,  2]
    FF_robust_times          = [1300, 41,  2]
    LSTM_nonrobust_times     = [220,  480,  340]
    LSTM_robust_times        = [2100, 4560, 3785]
    GRU_nonrobust_times      = [260,  720,  455]
    GRU_robust_times         = [2300, 2820, 4200]
    nonrobust_times = [S4_nonrobust_times, FF_nonrobust_times, LSTM_nonrobust_times, GRU_nonrobust_times]
    robust_times = [S4_robust_times, FF_robust_times, LSTM_robust_times, GRU_robust_times]
    #increase_in_computation_time(nonrobust_times, robust_times, labels)

    s4_nonrob_train = [0.2954, 0.4562, 0.5527, 0.6232, 0.6795, 0.7171, 0.7440, 0.7669, 0.7761, 0.7881, 0.7981, 0.8067, 0.8120, 0.8205, 0.8264, 0.8313]
    s4_nonrob_test  = [0.3935, 0.5114, 0.5877, 0.6531, 0.6862, 0.7258, 0.7466, 0.7560, 0.7425, 0.7640, 0.7742, 0.7727, 0.7952, 0.8043, 0.8104, 0.7995]
    s4_rob_train    = [0.1586, 0.1889, 0.2118, 0.2451, 0.2669, 0.2785, 0.3027, 0.3261, 0.3521, 0.3714, 0.3828, 0.4011, 0.4097, 0.4281, 0.4416, 0.4498]
    s4_rob_test     = [0.1804, 0.2792, 0.2688, 0.2447, 0.3389, 0.3379, 0.3768, 0.3882, 0.4242, 0.4589, 0.4621, 0.4857, 0.5292, 0.5215, 0.4816, 0.5522]
    accs = [s4_nonrob_train, s4_nonrob_test, s4_rob_train, s4_rob_test]
    labels = ["S4, nonrobust, train acc", "S4, nonrobust, test acc", "S4, robust, train acc", "S4, robust, test acc"]
    #plot_learning_accuracy(accs, labels=labels, title="Progress of accuracy, S4")
    gru_nonrob_train = [0.3047, 0.4579, 0.5126, 0.5893, 0.6606, 0.6987, 0.7353, 0.7649, 0.7864, 0.7719, 0.7251, 0.7920, 0.8106, 0.8337, 0.8413, 0.8524]
    gru_nonrob_test  = [0.3976, 0.4693, 0.5068, 0.5884, 0.6560, 0.6978, 0.7440, 0.7616, 0.7874, 0.7795, 0.7841, 0.6415, 0.8162, 0.8367, 0.8324, 0.8353]
    gru_rob_train    = [0.1455, 0.2174, 0.2654, 0.2946, 0.3157, 0.3452, 0.3638, 0.3836, 0.4115, 0.4448, 0.4600, 0.4903, 0.5165, 0.5404, 0.5669, 0.5819]
    gru_rob_test     = [0.1853, 0.2761, 0.2338, 0.2268, 0.2964, 0.2882, 0.2795, 0.3152, 0.3725, 0.4188, 0.3886, 0.3966, 0.4186, 0.4425, 0.4744, 0.4744]
    accs = [gru_nonrob_train, gru_nonrob_test, gru_rob_train, gru_rob_test]
    labels = ["GRU, nonrobust, train acc", "GRU, nonrobust, test acc", "GRU, robust, train acc", "GRU, robust, test acc"]
    #plot_learning_accuracy(accs, labels=labels, title="Progress of accuracy, GRU")
    lstm_nonrob_train = [0.2681, 0.4033, 0.4377, 0.4622, 0.5173, 0.4750, 0.5041, 0.4883, 0.4760, 0.5225, 0.5556, 0.5536, 0.5635, 0.4695, 0.4209, 0.5155]
    lstm_nonrob_test  = [0.3324, 0.4502, 0.4507, 0.4850, 0.3969, 0.4930, 0.5242, 0.4278, 0.4580, 0.5432, 0.5321, 0.5551, 0.5275, 0.3471, 0.4746, 0.5386]
    lstm_rob_train    = [0.1184, 0.1534, 0.1791, 0.2049, 0.2310, 0.2413, 0.2671, 0.2708, 0.2954, 0.2473, 0.2912, 0.3027, 0.3156, 0.3316, 0.3340, 0.3356]
    lstm_rob_test     = [0.1453, 0.2751, 0.2362, 0.1806, 0.2292, 0.2134, 0.3026, 0.2511, 0.2760, 0.3005, 0.2973, 0.3220, 0.2888, 0.2780, 0.2818, 0.2934]
    accs = [lstm_nonrob_train, lstm_nonrob_test, lstm_rob_train, lstm_rob_test]
    labels = ["LSTM, nonrobust, train acc", "LSTM, nonrobust, test acc", "LSTM, robust, train acc", "LSTM, robust, test acc"]
    #plot_learning_accuracy(accs, labels=labels, title="Progress of accuracy, LSTM")

if __name__ == "__main__":
    main()
