from flax import linen as nn

class FeedForwardModel(nn.Module):
    d_model: int
    N: int
    l_max: int
    decode: bool = False

    def setup(self):
        self.dense = nn.Dense(self.d_model)

    def __call__(self, x):
        """x - L x N"""
        return nn.relu(self.dense(x))